package fksteps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class FlipSteps {
	public RemoteWebDriver driver;
	@Given("Load CH Browser")
	public void loadCHBrowser() {
	 System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
	driver = new ChromeDriver();
	driver.get("https://www.flipkart.com/");
			}

	@Given("Maximize CH Browser")
	public void maximizeCHBrowser() {
	    driver.manage().window().maximize();
	
	}

	@Given("Close Login Popup")
	public void closePopup() {
	    driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
	    driver.getKeyboard().sendKeys(Keys.ESCAPE);
	}

	@Given("Hover On Electronics Link")
	public void hoverOnElectronicsLink() {
	    Actions action = new Actions(driver);
	    WebElement ele = driver.findElementByXPath("//span [text()=\"Electronics\"]");
	    action.moveToElement(ele).perform();
	}

	@Given("Click MI Link")
	public void clickMILink() {
		driver.findElementByLinkText("Mi").click();
	}

	@Then("Verify Phone Title Name")
	public void verifyPhoneTitleName() {
		System.out.println(driver.getTitle().contains("Mi Mobile")?"Title Verified":"Title Mismatch");
	}


}
