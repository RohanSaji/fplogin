package fkrunner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions(features="src\\main\\java\\flipfeature"
				,monochrome=true
				, glue = "FlipSteps"  
		)

public class FlipRunner {

}
   